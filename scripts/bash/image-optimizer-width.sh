#!/bin/sh

convert $1 -strip -interlace Plane -gaussian-blur 0.05 -resize $2x -quality $3 JPEG:$4