#!/bin/sh

ffmpeg -i $1 -preset slow -codec:a aac -b:a 128k -codec:v libx265 -pix_fmt yuv420p -b:v 750k -minrate 400k -maxrate 1000k -bufsize 1500k -vf scale=$2:$3 $4