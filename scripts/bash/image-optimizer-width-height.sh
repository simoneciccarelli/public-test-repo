#!/bin/sh

convert $1 -strip -interlace Plane -gaussian-blur 0.05 -resize $2x$3! -quality $4 JPEG:$5