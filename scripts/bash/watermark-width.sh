#!/bin/sh

DIM="$(convert $1 -format "%[fx:.$3*w]" info:)x"
convert $1 \( $2 -resize $DIM \) -gravity $4 -geometry +$5+$6 -composite -resize $7x -quality $8 JPEG:$9