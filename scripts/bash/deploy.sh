#!/usr/bin/env bash

# OPTIONAL take jenkins' build number from argument (12|13|15)
BUILD_NUM=$1
shift

if [ -z ${BUILD_NUM} ]; then
    BUILD_NUM=0
fi

APP_NAME=carspark-media-service
WEBAPP_FOLDER=/home/drivek/web/${APP_NAME}

TMP_WAR_NAME=${APP_NAME}
TMP_FOLDER=/tmp/${APP_NAME}
TOMCAT_INSTANCE_NAME=${APP_NAME}

function removeOldApplication {
    rm -rf ${WEBAPP_FOLDER}.old
}

function shuttingDownMonitService {
    sudo monit stop $1
}

function restartingMonitService {
    sudo monit start $1
}

function copyNewApplication {

    mv ${WEBAPP_FOLDER} ${WEBAPP_FOLDER}.old
    mkdir ${WEBAPP_FOLDER}
    cp ${TMP_FOLDER}/${APP_NAME}.NEW.war ${WEBAPP_FOLDER}/${APP_NAME}.war

    pushd ${WEBAPP_FOLDER} > /dev/null
    unzip -qq -o ${WEBAPP_FOLDER}/${APP_NAME}.war && rm -f ${WEBAPP_FOLDER}/${APP_NAME}.war
    sed -i 's/buildNumber=0/buildNumber='$BUILD_NUM'/g' ${WEBAPP_FOLDER}/WEB-INF/classes/version.properties
    popd > /dev/null
}

#####################################################################################
## Main

echo -e "Deploying ${TOMCAT_INSTANCE_NAME}"

shuttingDownMonitService "${TOMCAT_INSTANCE_NAME}"

removeOldApplication

copyNewApplication

restartingMonitService "${TOMCAT_INSTANCE_NAME}"

echo -e "Deploying ${TOMCAT_INSTANCE_NAME} - DONE"