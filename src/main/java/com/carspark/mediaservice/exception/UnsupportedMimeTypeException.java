package com.carspark.mediaservice.exception;

public class UnsupportedMimeTypeException extends RuntimeException {

    private static final String MESSAGE = "Unsupported Mime Type: %s";

    public UnsupportedMimeTypeException(String mimeType) {
        super(String.format(MESSAGE, mimeType));
    }
}
