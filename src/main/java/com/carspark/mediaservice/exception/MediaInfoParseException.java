package com.carspark.mediaservice.exception;

public class MediaInfoParseException extends RuntimeException {

    private static final String MESSAGE = "Some error occurs during the parsing of the of cmd response. ErrorMessage: %s";

    public MediaInfoParseException(String message) {
        super(String.format(MESSAGE, message));
    }
}
