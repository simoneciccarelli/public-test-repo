package com.carspark.mediaservice.exception;

import com.carspark.mediaservice.commons.WatermarkPosition;

public class InvalidWatermarkPositionException extends RuntimeException {

    private static final String MESSAGE = "Invalid watermark position: %s. Allowed values: %s";

    public InvalidWatermarkPositionException(String invalidPosition) {
        super(String.format(MESSAGE, invalidPosition, WatermarkPosition.allowedPositions()));
    }
}
