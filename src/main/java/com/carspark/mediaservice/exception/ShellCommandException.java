package com.carspark.mediaservice.exception;

public class ShellCommandException extends RuntimeException {

    private static final String MESSAGE = "Some error occurs during execution of cmd: %s";

    public ShellCommandException(String cmd) {
        super(String.format(MESSAGE, cmd));
    }
}
