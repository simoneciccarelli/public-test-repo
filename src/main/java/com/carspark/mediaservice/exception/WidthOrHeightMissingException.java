package com.carspark.mediaservice.exception;

public class WidthOrHeightMissingException extends RuntimeException {

    private static final String MESSAGE = "One between Width or Height is mandatory";

    public WidthOrHeightMissingException() {
        super(MESSAGE);
    }
}
