package com.carspark.mediaservice.commons;

public final class MediaServiceConstants {

    public final static String TYPE_AUDIO = "audio";
    public final static String TYPE_IMAGE = "image";
    public final static String TYPE_VIDEO = "video";

    public final static String AUDIO_MIME_TYPE = "audio/";
    public final static String IMAGE_MIME_TYPE = "image/";
    public final static String VIDEO_MIME_TYPE = "video/";

    public final static String JPG_EXTENSION = ".jpg";
    public final static String MP3_EXTENSION = ".mp3";
    public final static String MP4_EXTENSION = ".mp4";

    public final static String MEDIA_AREA_NAME_SPACE = "https://mediaarea.net/mediainfo";

    public final static String BITRATE_NODE_NAME = "BitRate";
    public final static String BITRATE_MODE_NODE_NAME = "BitRate_Mode";
    public final static String DURATION_NODE_NAME = "Duration";
    public final static String HEIGHT_NODE_NAME = "Height";
    public final static String MEDIA_NODE_NAME = "media";
    public final static String TRACK_NODE_NAME = "track";
    public final static String WIDTH_NODE_NAME = "Width";

}
