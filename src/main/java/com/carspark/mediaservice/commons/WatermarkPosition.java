package com.carspark.mediaservice.commons;

import com.carspark.mediaservice.exception.InvalidWatermarkPositionException;

public enum WatermarkPosition {

    NORTH_WEST("NorthWest"),
    NORTH("North"),
    NORTH_EAST("NorthEast"),
    WEST("West"),
    CENTER("Center"),
    EAST("East"),
    SOUTH_WEST("SouthWest"),
    SOUTH("South"),
    SOUTH_EAST("SouthEast");

    private String code;

    WatermarkPosition(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static WatermarkPosition fromCode(String position) {
        for (WatermarkPosition watermarkPosition : values()) {
            if (watermarkPosition.getCode().equalsIgnoreCase(position)) {
                return watermarkPosition;
            }
        }
        throw new InvalidWatermarkPositionException(position);
    }

    public static String allowedPositions() {
        StringBuilder allowedPositions = new StringBuilder();
        for (WatermarkPosition watermarkPosition : values()) {
            allowedPositions.append(watermarkPosition.getCode()).append(" ");
        }
        return allowedPositions.toString();
    }
}
