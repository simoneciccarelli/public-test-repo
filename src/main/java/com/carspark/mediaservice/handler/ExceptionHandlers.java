package com.carspark.mediaservice.handler;

import com.carspark.mediaservice.exception.InvalidWatermarkPositionException;
import com.carspark.mediaservice.exception.MediaInfoParseException;
import com.carspark.mediaservice.exception.ShellCommandException;
import com.carspark.mediaservice.exception.UnsupportedMimeTypeException;
import com.carspark.mediaservice.model.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice
public class ExceptionHandlers {

    private static Logger LOG = LoggerFactory.getLogger(ExceptionHandlers.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    protected ErrorResponse handleUncaughtException(Exception ex) {
        LOG.error(ex.getMessage(), ex);
        return new ErrorResponse(INTERNAL_SERVER_ERROR.name(), ex.getMessage(), ex);
    }

    @ExceptionHandler(ShellCommandException.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    protected ErrorResponse handleShellCommandException(ShellCommandException ex) {
        LOG.error(ex.getMessage());
        return new ErrorResponse(INTERNAL_SERVER_ERROR.name(), ex.getMessage(), ex);
    }

    @ExceptionHandler(MediaInfoParseException.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    protected ErrorResponse handleMediaInfoParseException(MediaInfoParseException ex) {
        LOG.error(ex.getMessage());
        return new ErrorResponse(INTERNAL_SERVER_ERROR.name(), ex.getMessage(), ex);
    }

    @ExceptionHandler(InvalidWatermarkPositionException.class)
    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    protected ErrorResponse handleInvalidWatermarkPositionException(InvalidWatermarkPositionException ex) {
        LOG.error(ex.getMessage());
        return new ErrorResponse(BAD_REQUEST.name(), ex.getMessage(), ex);
    }

    @ExceptionHandler(UnsupportedMimeTypeException.class)
    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    protected ErrorResponse handleUnsupportedMimeTypeException(UnsupportedMimeTypeException ex) {
        LOG.error(ex.getMessage());
        return new ErrorResponse(BAD_REQUEST.name(), ex.getMessage(), ex);
    }
}