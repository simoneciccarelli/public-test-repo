package com.carspark.mediaservice.model;

import io.swagger.annotations.ApiModelProperty;

public class WatermarkResponse {

    @ApiModelProperty("The resource's Id")
    public String id;

    @ApiModelProperty("The resource's MediaInfo input")
    public MediaInfoResponse input;

    @ApiModelProperty("The resource's MediaInfo in")
    public MediaInfoResponse watermark;

    @ApiModelProperty("The resource's MediaInfo output")
    public MediaInfoResponse output;

}
