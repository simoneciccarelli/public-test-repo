package com.carspark.mediaservice.model;

import io.swagger.annotations.ApiModelProperty;

public class MediaInfoResponse {

    @ApiModelProperty("The resource's Type")
    public String type;

    @ApiModelProperty("The resource's MimeType")
    public String mimeType;

    @ApiModelProperty("The resource's Width")
    public String width;

    @ApiModelProperty("The resource's Height")
    public String height;

    @ApiModelProperty("The resource's Duration")
    public String duration;

    @ApiModelProperty("The resource's Bitrate")
    public String bitrate;

    @ApiModelProperty("The resource's Bitrate Mode")
    public String bitrateMode;

}
