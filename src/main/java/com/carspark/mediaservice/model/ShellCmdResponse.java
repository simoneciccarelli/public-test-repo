package com.carspark.mediaservice.model;

public class ShellCmdResponse {

    int exitVal;
    String output;

    public ShellCmdResponse(int exitVal, String output) {
        this.exitVal = exitVal;
        this.output = output;
    }

    public int getExitVal() {
        return exitVal;
    }

    public void setExitVal(int exitVal) {
        this.exitVal = exitVal;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
}
