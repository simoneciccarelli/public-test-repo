package com.carspark.mediaservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import java.io.File;

@EnableScheduling
@SpringBootApplication
public class MediaServiceApplication {

    @Value("${media-service.path.in}")
    private String mediaServicePathIn;

    @Value("${media-service.path.out}")
    private String mediaServicePathOut;

    @Value("${media-service.path.watermark}")
    private String mediaServicePathWatermark;

    @PostConstruct
    private void init() {
        new File(mediaServicePathIn).mkdirs();
        new File(mediaServicePathOut).mkdirs();
        new File(mediaServicePathWatermark).mkdirs();
    }

    public static void main(String[] args) {
        SpringApplication.run(MediaServiceApplication.class, args);
    }
}
