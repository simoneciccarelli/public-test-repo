package com.carspark.mediaservice.controller;

import com.carspark.mediaservice.model.WatermarkResponse;
import com.carspark.mediaservice.commons.WatermarkPosition;
import com.carspark.mediaservice.service.WatermarkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/v1.0/private/watermark", produces = {MediaType.APPLICATION_JSON_VALUE})
@Api(value = "/v1.0/private", description = "Media Watermark operations", tags = {"Watermark"}, produces = "application/json")
public class WatermarkController {

    WatermarkService watermarkService;

    @Autowired
    public WatermarkController(WatermarkService watermarkService) {
        this.watermarkService = watermarkService;
    }

    @PostMapping(value = "/image", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Retrieve a media with an image watermark by configuration")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "watermark", value = "The watermark file that will be applied to the source", dataTypeClass = MultipartFile.class, required = true, paramType = "body"),
            @ApiImplicitParam(name = "watermarkRatio", value = "The percentage ratio between watermark and source width", dataTypeClass = String.class, required = true, paramType = "body"),
            @ApiImplicitParam(name = "source", value = "The source to apply the watermark", dataTypeClass = MultipartFile.class, required = true, paramType = "body"),
            @ApiImplicitParam(name = "position", value = "The position of the watermark", dataTypeClass = Integer.class, paramType = "body"),
            @ApiImplicitParam(name = "topBottom", value = "The topBottom of the watermark", dataTypeClass = Integer.class, paramType = "body"),
            @ApiImplicitParam(name = "leftRight", value = "The leftRight of the watermark", dataTypeClass = Integer.class, paramType = "body"),
            @ApiImplicitParam(name = "width", value = "The width of the watermark", dataTypeClass = Integer.class, paramType = "body"),
            @ApiImplicitParam(name = "height", value = "The height of the watermark", dataTypeClass = Integer.class, paramType = "body"),
            @ApiImplicitParam(name = "quality", value = "The quality of the watermark", dataTypeClass = Integer.class, paramType = "body")
    })
    public WatermarkResponse applyWatermark(@RequestParam("watermark") MultipartFile watermark,
                                            @RequestParam("watermarkRatio") String percentage,
                                            @RequestParam("source") MultipartFile source,
                                            @RequestParam(value = "position") String position,
                                            @RequestParam(value = "topBottom") Integer topBottom,
                                            @RequestParam(value = "leftRight") Integer leftRight,
                                            @RequestParam(value = "width", required = false) Integer width,
                                            @RequestParam(value = "height", required = false) Integer height,
                                            @RequestParam(value = "quality") Integer quality) throws Exception {

        return watermarkService.getWatermarkResponse(watermark, percentage, source, WatermarkPosition.fromCode(position), topBottom, leftRight, width, height, quality);
    }
}
