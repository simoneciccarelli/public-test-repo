package com.carspark.mediaservice.controller;

import com.carspark.mediaservice.model.OptimizerResponse;
import com.carspark.mediaservice.service.OptimizerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/v1.0/private", produces = {MediaType.APPLICATION_JSON_VALUE})
@Api(value = "/private", description = "Media optimizer operations", tags = {"Optimizer"}, produces = "application/json")
public class OptimizerController {

    OptimizerService optimizerService;

    @Autowired
    public OptimizerController(OptimizerService optimizerService) {
        this.optimizerService = optimizerService;
    }

    @PostMapping(value = "/optimizer", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Retrieve an optimized media by configuration")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "file", value = "The file that will be optimized", dataTypeClass = MultipartFile.class, required = true, paramType = "body"),
            @ApiImplicitParam(name = "height", value = "The height of the file", dataTypeClass = Integer.class, paramType = "body"),
            @ApiImplicitParam(name = "width", value = "The width of the file", dataTypeClass = Integer.class, paramType = "body"),
            @ApiImplicitParam(name = "quality", value = "The quality of the file", dataTypeClass = Integer.class, paramType = "body")
    })
    public OptimizerResponse optimize(@RequestParam(value = "file") MultipartFile file,
                                      @RequestParam(value = "height", required = false) Integer height,
                                      @RequestParam(value = "width", required = false) Integer width,
                                      @RequestParam(value = "quality", required = false) Integer quality) throws Exception {

        return optimizerService.getOptimizerResponse(file, height, width, quality);
    }
}
