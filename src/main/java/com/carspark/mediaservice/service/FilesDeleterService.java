package com.carspark.mediaservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Set;

@Component
public class FilesDeleterService {

    private static Logger LOG = LoggerFactory.getLogger(FilesDeleterService.class);

    private static final int SCHEDULED_EVERY_MINUTES = 600000;
    private static final int EXPIRED_AFTER_MINUTES = 10;

    @Value("${media-service.path.in}")
    private String mediaServicePathIn;

    @Value("${media-service.path.out}")
    private String mediaServicePathOut;

    @Value("${media-service.path.watermark}")
    private String mediaServicePathWatermark;

    @Scheduled(fixedRate = SCHEDULED_EVERY_MINUTES)
    public void run() {

        Instant now = Instant.now();
        Path inPath = Paths.get(mediaServicePathIn);
        Path outPath = Paths.get(mediaServicePathOut);
        Path watermarkPath = Paths.get(mediaServicePathWatermark);
        Set<Path> toBeDeleted = new HashSet<>();

        try {
            toBeDeleted.addAll(listExpiredFiles(inPath, now));
            toBeDeleted.addAll(listExpiredFiles(outPath, now));
            toBeDeleted.addAll(listExpiredFiles(watermarkPath, now));
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            for (Path path : toBeDeleted) {
                try {
                    boolean deleted = Files.deleteIfExists(path);
                    if (deleted) {
                        LOG.info("Deleted file: {}", path.toAbsolutePath().toString());
                    }
                } catch (IOException e) {
                    LOG.error(e.getMessage(), e);
                }
            }
        }
    }

    public Set<Path> listExpiredFiles(Path dir, Instant now) throws IOException {

        Set<Path> toBeDeleted = new HashSet<>();
        Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                if (!Files.isDirectory(file)) {
                    Instant createdAt = attrs.creationTime().toInstant();
                    Instant expiredAt = createdAt.plus(EXPIRED_AFTER_MINUTES, ChronoUnit.MINUTES);
                    if (now.isAfter(expiredAt)) {
                        toBeDeleted.add(file);
                    }
                }
                return FileVisitResult.CONTINUE;
            }
        });
        return toBeDeleted;
    }
}
