package com.carspark.mediaservice.service;

import com.carspark.mediaservice.exception.UnsupportedMimeTypeException;
import com.carspark.mediaservice.model.OptimizerResponse;
import com.carspark.mediaservice.service.shell.MediaInfoProxy;
import com.carspark.mediaservice.service.shell.MimeTypeProxy;
import com.carspark.mediaservice.service.shell.AudioOptimizerProxy;
import com.carspark.mediaservice.service.shell.ImageOptimizerProxy;
import com.carspark.mediaservice.service.shell.VideoOptimizerProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static com.carspark.mediaservice.commons.MediaServiceConstants.*;

@Service
public class OptimizerService {

    @Value("${media-service.path.in}")
    private String mediaServicePathIn;

    @Value("${media-service.path.out}")
    private String mediaServicePathOut;

    private final AudioOptimizerProxy audioOptimizerProxy;
    private final ImageOptimizerProxy imageOptimizerProxy;
    private final MediaInfoProxy mediaInfoProxy;
    private final MimeTypeProxy mimeTypeProxy;
    private final VideoOptimizerProxy videoOptimizerProxy;

    @Autowired
    public OptimizerService(AudioOptimizerProxy audioOptimizerProxy, ImageOptimizerProxy imageOptimizerProxy, MediaInfoProxy mediaInfoProxy, MimeTypeProxy mimeTypeProxy, VideoOptimizerProxy videoOptimizerProxy) {
        this.audioOptimizerProxy = audioOptimizerProxy;
        this.imageOptimizerProxy = imageOptimizerProxy;
        this.mediaInfoProxy = mediaInfoProxy;
        this.mimeTypeProxy = mimeTypeProxy;
        this.videoOptimizerProxy = videoOptimizerProxy;
    }

    public OptimizerResponse getOptimizerResponse(MultipartFile file, Integer height, Integer width, Integer quality) throws Exception {

        String uuid = UUID.randomUUID().toString();
        Path inPath = Paths.get(mediaServicePathIn + uuid);
        Path outPath;

        Files.copy(file.getInputStream(), inPath);

        String mimeType = mimeTypeProxy.getMimeType(inPath);

        if (mimeType.startsWith(IMAGE_MIME_TYPE)) {
            outPath = Paths.get(mediaServicePathOut + uuid + JPG_EXTENSION);
            imageOptimizerProxy.optimizeImage(inPath, width, height, quality, outPath);
        } else if (mimeType.startsWith(AUDIO_MIME_TYPE)) {
            outPath = Paths.get(mediaServicePathOut + uuid + MP3_EXTENSION);
            audioOptimizerProxy.optimizeAudio(inPath, quality, outPath);
        } else if (mimeType.startsWith(VIDEO_MIME_TYPE)) {
            outPath = Paths.get(mediaServicePathOut + uuid + MP4_EXTENSION);
            videoOptimizerProxy.optimizeVideo(inPath, width, height, outPath);
        } else {
            throw new UnsupportedMimeTypeException(mimeType);
        }

        OptimizerResponse optimizerResponse = new OptimizerResponse();
        optimizerResponse.id = uuid;
        optimizerResponse.input = mediaInfoProxy.getMediaInfo(inPath, mimeType);
        optimizerResponse.output = mediaInfoProxy.getMediaInfo(outPath, mimeType);
        return optimizerResponse;
    }
}
