package com.carspark.mediaservice.service.shell;

import com.carspark.mediaservice.exception.WidthOrHeightMissingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Path;

@Service
public class VideoOptimizerProxy {

    @Value("${media-service.script.video-optimizer.width-height}")
    private String scriptWidthHeight;
    @Value("${media-service.script.video-optimizer.width}")
    private String scriptWidth;
    @Value("${media-service.script.video-optimizer.height}")
    private String scriptHeight;

    private final ShellCmdExecutorService shellCmdExecutorService;

    @Autowired
    public VideoOptimizerProxy(ShellCmdExecutorService shellCmdExecutorService) {
        this.shellCmdExecutorService = shellCmdExecutorService;
    }

    public void optimizeVideo(Path inPath, Integer width, Integer height, Path outPath) throws Exception {
        if (width == null && height == null) {
            throw new WidthOrHeightMissingException();
        }

        if (width == null) {
            shellCmdExecutorService.exec(scriptHeight, inPath.toAbsolutePath().toString(), height.toString(), outPath.toAbsolutePath().toString());
            return;
        }

        if (height == null) {
            shellCmdExecutorService.exec(scriptWidth, inPath.toAbsolutePath().toString(), width.toString(), outPath.toAbsolutePath().toString());
            return;
        }

        shellCmdExecutorService.exec(scriptWidthHeight, inPath.toAbsolutePath().toString(), width.toString(), height.toString(), outPath.toAbsolutePath().toString());
    }
}
