package com.carspark.mediaservice.service.shell;

import com.carspark.mediaservice.commons.WatermarkPosition;
import com.carspark.mediaservice.exception.WidthOrHeightMissingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Path;

@Service
public class WatermarkProxy {

    @Value("${media-service.script.watermark.width-height}")
    private String scriptWidthHeight;
    @Value("${media-service.script.watermark.width}")
    private String scriptWidth;
    @Value("${media-service.script.watermark.height}")
    private String scriptHeight;

    private final ShellCmdExecutorService shellCmdExecutorService;

    @Autowired
    public WatermarkProxy(ShellCmdExecutorService shellCmdExecutorService) {
        this.shellCmdExecutorService = shellCmdExecutorService;
    }


    public void applyWatermark(Path inPath, Path watermarkPath, String percentage, WatermarkPosition position, Integer topBottom, Integer leftRight, Integer width, Integer height, Integer quality, Path outPath) throws Exception {

        if (width == null && height == null) {
            throw new WidthOrHeightMissingException();
        }

        if (width == null) {
            shellCmdExecutorService.exec(scriptHeight, inPath.toAbsolutePath().toString(), watermarkPath.toAbsolutePath().toString(), String.valueOf(percentage), position.getCode(),  String.valueOf(topBottom), String.valueOf(leftRight), height.toString(), String.valueOf(quality), outPath.toAbsolutePath().toString());
            return;
        }

        if (height == null) {
            shellCmdExecutorService.exec(scriptWidth, inPath.toAbsolutePath().toString(), watermarkPath.toAbsolutePath().toString(),  String.valueOf(percentage), position.getCode(), String.valueOf(topBottom), String.valueOf(leftRight), width.toString(), String.valueOf(quality), outPath.toAbsolutePath().toString());
            return;
        }

        shellCmdExecutorService.exec(scriptWidthHeight, inPath.toAbsolutePath().toString(), watermarkPath.toAbsolutePath().toString(),  String.valueOf(percentage), position.getCode(), String.valueOf(topBottom), String.valueOf(leftRight), width.toString(), height.toString(), String.valueOf(quality), outPath.toAbsolutePath().toString());
    }
}
