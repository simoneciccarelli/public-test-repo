package com.carspark.mediaservice.service.shell;

import com.carspark.mediaservice.exception.WidthOrHeightMissingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Path;

@Service
public class ImageOptimizerProxy {

    @Value("${media-service.script.image-optimizer.width-height}")
    private String scriptWidthHeight;
    @Value("${media-service.script.image-optimizer.width}")
    private String scriptWidth;
    @Value("${media-service.script.image-optimizer.height}")
    private String scriptHeight;

    private final ShellCmdExecutorService shellCmdExecutorService;

    @Autowired
    public ImageOptimizerProxy(ShellCmdExecutorService shellCmdExecutorService) {
        this.shellCmdExecutorService = shellCmdExecutorService;
    }

    public void optimizeImage(Path inPath, Integer width, Integer height, int quality, Path outPath) throws Exception {
        if (width == null && height == null) {
            throw new WidthOrHeightMissingException();
        }

        if (width == null) {
            shellCmdExecutorService.exec(scriptHeight, inPath.toAbsolutePath().toAbsolutePath().toString(), height.toString(), String.valueOf(quality), outPath.toAbsolutePath().toString());
            return;
        }

        if (height == null) {
            shellCmdExecutorService.exec(scriptWidth, inPath.toAbsolutePath().toString(), width.toString(), String.valueOf(quality), outPath.toAbsolutePath().toString());
            return;
        }

        shellCmdExecutorService.exec(scriptWidthHeight, inPath.toAbsolutePath().toString(), width.toString(), height.toString(), String.valueOf(quality), outPath.toAbsolutePath().toString());
    }
}
