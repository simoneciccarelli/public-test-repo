package com.carspark.mediaservice.service.shell;

import com.carspark.mediaservice.model.MediaInfoResponse;
import com.carspark.mediaservice.model.ShellCmdResponse;
import com.carspark.mediaservice.service.converter.MediaInfoConverterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Path;

@Service
public class MediaInfoProxy {

    @Value("${media-service.script.media-info}")
    private String scriptSh;

    private final ShellCmdExecutorService shellCmdExecutorService;
    private final MediaInfoConverterService mediaInfoConverterService;

    @Autowired
    public MediaInfoProxy(ShellCmdExecutorService shellCmdExecutorService, MediaInfoConverterService mediaInfoConverterService) {
        this.shellCmdExecutorService = shellCmdExecutorService;
        this.mediaInfoConverterService = mediaInfoConverterService;
    }

    public MediaInfoResponse getMediaInfo(Path inPath, String mimeType) {
        ShellCmdResponse shellCmdResponse = shellCmdExecutorService.exec(scriptSh, inPath.toAbsolutePath().toString());
        return mediaInfoConverterService.convert(shellCmdResponse, mimeType);
    }
}
