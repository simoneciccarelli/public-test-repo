package com.carspark.mediaservice.service.shell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Path;

@Service
public class AudioOptimizerProxy {

    @Value("${media-service.script.audio-optimizer}")
    private String scriptSh;

    private final ShellCmdExecutorService shellCmdExecutorService;

    @Autowired
    public AudioOptimizerProxy(ShellCmdExecutorService shellCmdExecutorService) {
        this.shellCmdExecutorService = shellCmdExecutorService;
    }

    public void optimizeAudio(Path inPath, Integer quality, Path outPath){
        shellCmdExecutorService.exec(scriptSh, inPath.toAbsolutePath().toString(), String.valueOf(quality), outPath.toAbsolutePath().toString());
    }
}
