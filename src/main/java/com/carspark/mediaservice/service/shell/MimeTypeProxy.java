package com.carspark.mediaservice.service.shell;

import com.carspark.mediaservice.model.ShellCmdResponse;
import com.carspark.mediaservice.service.converter.MimeTypeConverterService;

import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MimeTypeProxy {

    @Value("${media-service.script.mime-type}")
    private String script;

    private final ShellCmdExecutorService shellCmdExecutorService;
    private final MimeTypeConverterService mimeTypeConverterService;

    @Autowired
    public MimeTypeProxy(ShellCmdExecutorService shellCmdExecutorService, MimeTypeConverterService mimeTypeConverterService) {
        this.shellCmdExecutorService = shellCmdExecutorService;
        this.mimeTypeConverterService = mimeTypeConverterService;
    }

    public String getMimeType(Path inPath) {
        ShellCmdResponse shellCmdResponse = shellCmdExecutorService.exec(script, inPath.toAbsolutePath().toString());
        return mimeTypeConverterService.convert(shellCmdResponse);
    }
}
