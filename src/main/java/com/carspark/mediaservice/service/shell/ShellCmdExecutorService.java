package com.carspark.mediaservice.service.shell;

import com.carspark.mediaservice.exception.ShellCommandException;
import com.carspark.mediaservice.model.ShellCmdResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Service
public class ShellCmdExecutorService {

    Logger LOG = LoggerFactory.getLogger(ShellCmdExecutorService.class);

    public ShellCmdResponse exec(String... args) {

        LOG.debug("Executing shName: {}", args[0]);

        try {
            ProcessBuilder processBuilder = new ProcessBuilder(args);
            Process process = processBuilder.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            StringBuilder output = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }

            int exitVal = process.waitFor();
            if (exitVal == 0) {
                LOG.debug("Cmd completed. Output: {}", output.toString());
            } else {
                LOG.debug("Cmd failed. Output: {}", output.toString());
            }

            return new ShellCmdResponse(exitVal, output.toString());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new ShellCommandException(args[0]);
        }
    }
}
