package com.carspark.mediaservice.service.converter;

import com.carspark.mediaservice.model.ShellCmdResponse;
import org.springframework.stereotype.Service;

@Service
public class MimeTypeConverterService {

    public String convert(ShellCmdResponse shellCmdResponse) {
        if (shellCmdResponse.getExitVal() == 0) {
            return shellCmdResponse.getOutput().split("\\s+")[1].trim();
        } else {
            return null;
        }
    }
}
