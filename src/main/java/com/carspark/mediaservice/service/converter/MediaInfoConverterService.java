package com.carspark.mediaservice.service.converter;

import com.carspark.mediaservice.exception.MediaInfoParseException;
import com.carspark.mediaservice.model.MediaInfoResponse;
import com.carspark.mediaservice.model.ShellCmdResponse;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.input.DOMBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.List;

import static com.carspark.mediaservice.commons.MediaServiceConstants.*;

@Service
public class MediaInfoConverterService {

    Logger LOG = LoggerFactory.getLogger(MediaInfoConverterService.class);

    public MediaInfoResponse convert(ShellCmdResponse shellCmdResponse, String mimeType) {

        if (shellCmdResponse.getExitVal() == 0) {
            try {
                MediaInfoResponse mediaInfoResponse = new MediaInfoResponse();
                Namespace defaultNS = Namespace.getNamespace(MEDIA_AREA_NAME_SPACE);

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(new InputSource(new StringReader(shellCmdResponse.getOutput())));

                org.jdom2.Document dom = new DOMBuilder().build(document);
                Element root = dom.getRootElement();
                Element media = root.getChild(MEDIA_NODE_NAME, defaultNS);

                Element image = null;
                Element audio = null;
                Element video = null;

                List<Element> trackList = media.getChildren(TRACK_NODE_NAME, defaultNS);
                for (Element track : trackList) {
                    if (mimeType.startsWith(IMAGE_MIME_TYPE)) {
                        image = track;
                        mediaInfoResponse.type = TYPE_IMAGE;
                    } else if (mimeType.startsWith(AUDIO_MIME_TYPE)) {
                        audio = track;
                        mediaInfoResponse.type = TYPE_AUDIO;
                    } else if (mimeType.startsWith(VIDEO_MIME_TYPE)) {
                        video = track;
                        mediaInfoResponse.type = TYPE_VIDEO;
                    }
                }

                if (image != null) {
                    mediaInfoResponse.width = image.getChild(WIDTH_NODE_NAME, defaultNS).getValue().toLowerCase();
                    mediaInfoResponse.height = image.getChild(HEIGHT_NODE_NAME, defaultNS).getValue().toLowerCase();
                }

                if (audio != null) {
                    mediaInfoResponse.duration = audio.getChild(DURATION_NODE_NAME, defaultNS).getValue().toLowerCase();
                    mediaInfoResponse.bitrate = audio.getChild(BITRATE_NODE_NAME, defaultNS).getValue().toLowerCase();
                    mediaInfoResponse.bitrateMode = audio.getChild(BITRATE_MODE_NODE_NAME, defaultNS).getValue().toLowerCase();
                }

                if (video != null) {
                    mediaInfoResponse.width = video.getChild(WIDTH_NODE_NAME, defaultNS).getValue().toLowerCase();
                    mediaInfoResponse.height = video.getChild(HEIGHT_NODE_NAME, defaultNS).getValue().toLowerCase();
                    mediaInfoResponse.duration = video.getChild(DURATION_NODE_NAME, defaultNS).getValue().toLowerCase();
                    mediaInfoResponse.bitrate = video.getChild(BITRATE_NODE_NAME, defaultNS).getValue().toLowerCase();
                }

                mediaInfoResponse.mimeType = mimeType;

                return mediaInfoResponse;
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                throw new MediaInfoParseException(e.getMessage());
            }
        } else {
            return null;
        }
    }
}
