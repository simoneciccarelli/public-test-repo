package com.carspark.mediaservice.service;

import com.carspark.mediaservice.commons.WatermarkPosition;
import com.carspark.mediaservice.model.WatermarkResponse;
import com.carspark.mediaservice.service.shell.*;
import com.carspark.mediaservice.service.shell.MediaInfoProxy;
import com.carspark.mediaservice.service.shell.MimeTypeProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import static com.carspark.mediaservice.commons.MediaServiceConstants.JPG_EXTENSION;

@Service
public class WatermarkService {

    @Value("${media-service.path.in}")
    private String mediaServicePathIn;

    @Value("${media-service.path.out}")
    private String mediaServicePathOut;

    @Value("${media-service.path.watermark}")
    private String mediaServicePathWatermark;

    private final MediaInfoProxy mediaInfoProxy;
    private final MimeTypeProxy mimeTypeProxy;
    private final WatermarkProxy watermarkProxy;

    @Autowired
    public WatermarkService(MediaInfoProxy mediaInfoProxy, MimeTypeProxy mimeTypeProxy, WatermarkProxy watermarkProxy) {
        this.mediaInfoProxy = mediaInfoProxy;
        this.mimeTypeProxy = mimeTypeProxy;
        this.watermarkProxy = watermarkProxy;
    }

    public WatermarkResponse getWatermarkResponse(MultipartFile watermark, String percentage, MultipartFile source, WatermarkPosition position, Integer topBottom, Integer leftRight, Integer with, Integer height, Integer quality) throws Exception {

        String uuidWatermark = UUID.randomUUID().toString();
        String uuidSource = UUID.randomUUID().toString();
        String uuidOutput = UUID.randomUUID().toString();

        Path inPath = Paths.get(mediaServicePathIn + uuidSource);
        Path outPath = Paths.get(mediaServicePathOut + uuidOutput + JPG_EXTENSION);
        Path watermarkPath = Paths.get(mediaServicePathWatermark + uuidWatermark);
        Files.copy(watermark.getInputStream(), watermarkPath);
        Files.copy(source.getInputStream(), inPath);

        watermarkProxy.applyWatermark(inPath, watermarkPath, percentage, position, topBottom, leftRight, with, height, quality, outPath);

        String mimeTypeIn = mimeTypeProxy.getMimeType(inPath);
        String mimeTypeOut = mimeTypeProxy.getMimeType(outPath);
        String mimeTypeWatermark = mimeTypeProxy.getMimeType(watermarkPath);

        WatermarkResponse watermarkResponse = new WatermarkResponse();
        watermarkResponse.id = uuidOutput;
        watermarkResponse.input = mediaInfoProxy.getMediaInfo(inPath, mimeTypeIn);
        watermarkResponse.output = mediaInfoProxy.getMediaInfo(outPath, mimeTypeOut);
        watermarkResponse.watermark = mediaInfoProxy.getMediaInfo(watermarkPath, mimeTypeWatermark);
        return watermarkResponse;
    }
}
