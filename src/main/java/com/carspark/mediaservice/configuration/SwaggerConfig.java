package com.carspark.mediaservice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("media-service-1.0")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.carspark.mediaservice.controller"))
                .paths(regex("/v1.0.*"))
                .build()
                .apiInfo(apiInfo("1.0"));
    }

    private ApiInfo apiInfo(String version) {
        return new ApiInfo(
                "Carspark Media Service",
                "Carspark Media Service",
                version,
                "",
                new Contact("Carspark Team", "https://www.motork.io/", "carspark-team@drivek.com"),
                "©MotorK",
                "https://www.motork.io/",
                Collections.emptyList()
        );
    }
}

